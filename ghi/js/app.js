window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url); //Gets data from the URL

        if (!response.ok) {
            // Figure out what to do when the response is bad.
        } else {
            const data = await response.json(); //turns the URL data into JSON

            const conference = data.conferences[0]; //gets first instance of conferences
            const nameTag = document.querySelector('.card-title'); //assigns var to the element with card-title class
            nameTag.innerHTML = conference.name; //lets you use name attribute of conference as dynamic html

            const detailUrl = `http://localhost:8000${conference.href}`; //accesses the conference data to get the href property
            const detailResponse = await fetch(detailUrl); //gets the data from the conference.href
            if (detailResponse.ok) { //if fetch is successful, shows the JSON data
                const details = await detailResponse.json();
                const conferenceDetails = details.conference;
                const descriptionTag = document.querySelector('.card-text');
                descriptionTag.innerHTML = conferenceDetails.description;

                const imageTag = document.querySelector('.card-img-top');
                imageTag.src = conferenceDetails.location.picture_url;
            }

        }
    } catch (e) {
        //Figure out what to do if an error is raised.
    }

});
